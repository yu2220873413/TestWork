## 数据库建模工具 pdman

下载地址:http://www.pdman.cn/

### [阮一峰的网络日志](https://www.baidu.com/link?url=APtC8ZP5N5p0h-Q40LylPk71qgynGgcf1VRz3xBhjWUR3JkrLRUfbQneWZ8nokKSPmlFFiH1wJxqtsA2zR64mK&wd=&eqid=d46496590000ad28000000066097d9a4)





## git 忽略文件

https://mp.weixin.qq.com/s/Bf7uVhGiu47uOELjmC5uXQ



```java
*.class
*.log
*.lock

# Package Files #
*.jar
*.war
*.ear
target/

# idea
.idea/
*.iml/

*velocity.log*

### STS ###
.apt_generated
.factorypath
.springBeans

### IntelliJ IDEA ###
*.iml
*.ipr
*.iws
.idea
.classpath
.project
.settings/
bin/

*.log
tem/

#rebel
*rebel.xml*
```

## git 操作命令

``` yaml
git branch -r查看不了远程所有分支
若部分没有展示，则需要先fetch下，如下
git fetch <远程仓库别名> 
例如 git fetch  origin

# 列出所有本地分支
git branch
# 列出所有远程分支
git branch -r
# 新建一个分支，但依然停留在当前分支
git branch [branch-name]
# 新建一个分支，并切换到该分支
git checkout -b [branch]
# 合并指定分支到当前分支
$ git merge [branch]
# 删除分支
$ git branch -d [branch-name]
# 删除远程分支
$ git push origin --delete [branch-name]
$ git branch -dr [remote/branch]




```

#### 切换分支

> **新建仓库后，默认生成了master分支** 

- **如果你想新建分支并切换**

```java
$ git checkout -b <new-branch-name>
# 例如 git checkout -b dev
# 如果仅新建，不切换，则去掉参数 -b

```

- **看看当前有哪些分支**

```java
$ git branch
# * dev
#   master # 标*号的代表当前所在的分支
```

- **看看当前本地&远程有哪些分支**

```java
$ git branch -a
# * dev
#   master
#   remotes/origin/master
```

- **切换到现有的分支**

```java
$ git checkout master
```

- **你想把dev分支合并到master分支**

```java
$ git merge <branch-name>
# 例如 git merge dev
```

- **你想把本地master分支推送到远程去**

```java
$ git push origin master
# 你可以使用git push -u origin master将本地分支与远程分支关联，之后仅需要使用git push即可。

```

- **远程分支被别人更新了，你需要更新代码**

```java
$ git pull origin <branch-name>
# 之前如果push时使用过-u，那么就可以省略为git pull

```

- **本地有修改，能不能先git pull**

```java
$ git stash # 工作区修改暂存
$ git pull  # 更新分支
$ git stash pop # 暂存修改恢复到工作区
```

### 拉取指定分支

```java
git clone -b dev 代码仓库地址 （dev是分支名称）
```

### 删除git中缓存的用户名和密码

网址:https://www.jianshu.com/p/b49f6dfbf721

```java
运行一下命令缓存输入的用户名和密码：
git config --global credential.helper wincred
清除掉缓存在git中的用户名和密码
git credential-manager uninstall

在推送到远端
```

git push 远端地址

可能还会要求输入用户名和密码

### 设置本机绑定SSH公钥，实现免密码登录！

```java
# 进入 C:\Users\Administrator\.ssh 目录
# 生成公钥
ssh-keygen -t rsa
将公钥信息public key 添加到码云账户中即可！


```

