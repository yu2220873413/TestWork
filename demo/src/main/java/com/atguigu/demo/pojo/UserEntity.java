package com.atguigu.demo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.annotation.entity.AutoID;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name="sys_user")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
    @AutoID
    private Integer id;
    private String name;
    private Integer departmentId;
}