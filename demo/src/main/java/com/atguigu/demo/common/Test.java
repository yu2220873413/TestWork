package com.atguigu.demo.common;

public class Test {
    public static void main(String[] args) {
        OneClass oneClass = new OneClass();
        ImTheOne theOne = oneClass::concatString;
        String result = theOne.handleString("abc", "def");
        System.out.println(result);
 
        //相当于以下效果
        OneClass oneClass2 = new OneClass();
        ImTheOne theOne2 = (a, b) -> oneClass2.concatString(a, b);
        String result2 = theOne2.handleString("123", "456");
        System.out.println(result2);



        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        new Thread(()->{

        });

        ImTheOne theOne3 = (a, b) ->  a+b;
    }
}