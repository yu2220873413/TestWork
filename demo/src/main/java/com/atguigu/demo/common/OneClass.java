package com.atguigu.demo.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class OneClass {
    private String name;
    public String concatString(String a, String b) {
        return a + b;
    }
}
 