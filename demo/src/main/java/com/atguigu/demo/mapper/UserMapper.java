package com.atguigu.demo.mapper;

import com.atguigu.demo.pojo.DepartmentEntity;
import com.atguigu.demo.pojo.UserEntity;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SqlResource("user")
public interface UserMapper extends BaseMapper<UserEntity> {

    /**
     * 调用sql文件user.md#select,方法名即markdown片段名字
     * @param name
     * @return
     */
    List<UserEntity> select(@Param("name") String name);


    @Sql("select * from sys_user where id = ?")
    @Select
    UserEntity queryUserById(Integer id);

    @Sql("update sys_user set name=? where id = ?")
    @Update
    int updateName(String name,Integer id);

    @Template("select * from sys_user where id = #{id}")
    UserEntity getUserById(Integer id);

    @SpringData
    List<UserEntity> queryByNameOrderById(String name);


    /**
     * 可以定义一个default接口
     * @return
     */
    default  List<DepartmentEntity> findAllDepartment(){
        Map paras = new HashMap();
        paras.put("exlcudeId",1);
        List<DepartmentEntity> list = getSQLManager().execute("select * from department where id != #{exlcudeId}",DepartmentEntity.class,paras);
        return list;
    }

}