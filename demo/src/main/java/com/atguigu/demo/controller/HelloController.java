package com.atguigu.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: 余希瑶
 * @date: 2021年05月08日 15:41
 * @version:1.0
 */
@RestController()
public class HelloController {

    @GetMapping("/hel")
    public String hello(){
        return "ok111";
    }
}
