package com.java1234.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: 余希瑶
 * @date: 2021年05月12日 1:46
 * @version:1.0
 */
@RestController
@RequestMapping("/nacos")
@RefreshScope
public class NacosConfigController {

    @Value("${java1234.name}")
    private String name;

    @Value("${java1234.age}")
    private String age;

    @GetMapping("/getConfigInfo")
    public  String getConfigInfo(){
        return name+":"+age;
    }
}
